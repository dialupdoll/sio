package io.gitlab.dahlterm.implementation.protocols.listpad;

import java.awt.Component;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.UUID;

import javax.swing.BorderFactory;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;

import com.grack.nanojson.JsonAppendableWriter;
import com.grack.nanojson.JsonWriter;

import io.gitlab.dahlterm.abstractions.Action;
import io.gitlab.dahlterm.abstractions.LayoutNode;
import io.gitlab.dahlterm.abstractions.ObjectCapabilities;
import io.gitlab.dahlterm.abstractions.events.EventReciever;
import io.gitlab.dahlterm.abstractions.protocol.Account;
import io.gitlab.dahlterm.abstractions.protocol.Channel;
import io.gitlab.dahlterm.abstractions.protocol.ContentPage;
import io.gitlab.dahlterm.abstractions.protocol.Message;
import io.gitlab.dahlterm.abstractions.protocol.Protocol;
import io.gitlab.dahlterm.implementation.protocols.listpad.FakeAccountListpad.ListpadNote;

public class FakeAccountListpad implements Account {
	
	String accountName;
	
	Protocol parentProtocol = null;
	
	public ArrayList<ListpadNote> notes = new ArrayList<FakeAccountListpad.ListpadNote>();
	
	public class ListpadNote {
		String title;
		String topic;
		ArrayList<String> messages = new ArrayList<String>();
		public ListpadNote() {
			
		}
	}
	public ListpadNote create() {
		return new ListpadNote();
	}
	
	public FakeAccountListpad(Protocol creator) {
		accountName = "Default Notepad Account";
		parentProtocol = creator;
	}

	public Action[] properties() {
		// we dont need any settings for the account itself
		return null;
	}

	public Channel[] availableChannels() {
		Channel[] avail = new Channel[notes.size()];
		int x = 0;
		for(ListpadNote note : notes) {
			avail[x] = forNote(note, null);
			x++;
		}
		return avail;
	}
	public String nickname() {
		return accountName;
	}

	public ArrayList<LayoutNode> defaultHierarchys() {
		// the listpad is too simple a protocol to have anything like contacts to return default hierarchies for
		return null;
	}
	
	public Component accountSettingsConfigurationMenu(EventReciever uer) {

		JPanel accountSettings = new JPanel();
		accountSettings.setBorder(BorderFactory.createLoweredBevelBorder());
		accountSettings.setLayout(new GridLayout(0,1));
		
		JPanel reusedVariable = null; //reused once per setting
		reusedVariable = new JPanel();
		JTextField editNoteAccountName = new JTextField(20);
		editNoteAccountName.setText(accountName);
		final JTextField editedName = editNoteAccountName;
		final FakeAccountListpad acc = this;
		final EventReciever event = uer;
		editNoteAccountName.getDocument().addDocumentListener(new DocumentListener() {
			
			public void removeUpdate(DocumentEvent e) {
				changed();
			}
			
			public void insertUpdate(DocumentEvent e) {
				changed();
			}
			
			public void changedUpdate(DocumentEvent e) {
				changed();				
			}
			void changed() {
				acc.accountName = editedName.getText();
				event.receiveEvent(null);
			}
		});
		reusedVariable.add(editNoteAccountName);
		accountSettings.add(reusedVariable);
		
		return accountSettings;
	}
	
	UUID ident = null;

	public UUID identifier() {
		if(ident ==null)
			ident = UUID.randomUUID();
		return ident;
	}
	
	public Channel forNote(ListpadNote note, LayoutNode cont_node) {
		final LayoutNode passNode = cont_node;
		final ListpadNote passNote = note;
		final Account owningAccount = this;

		return new Channel() {
			UUID ident;
			LayoutNode node = passNode;
			EventReciever updater;

			public UUID identifier() {
				if(ident ==null)
					ident = UUID.randomUUID();
				return ident;
			}
			
			public Action[] properties() {
				Action setTopic = new Action() {

					public String actionTitle() {
						return "set new chatroom topic";
					}

					public void performAction(ObjectCapabilities target) {
						String response = JOptionPane.showInputDialog(null,
								 "What should the new Topic be?",
								 "Enter your new topic",
								 JOptionPane.QUESTION_MESSAGE);
						passNote.topic = response;
						updater.receiveEvent(null);
					}

					public String helpInformation() {
						return "Lets you set new chatroom topic.";
					}

					public UUID identifier() {
						return null;
					}
					
				};
				
				return new Action[] { setTopic };
			}
			
			public String topic() {
				return passNote.topic;
			}
			
			public String title() {
				return passNote.title;
			}
			
			public void setUpdateReciever(EventReciever recipient) {
				updater = recipient;
			}
			
			public void send(Message m) {
				passNote.messages.add(m.asText());
				updater.receiveEvent(null);
			}
			
			public ContentPage retrievePage(ContentPage previous) {
				return new ContentPage() {
					
					public int notificationBadge() {
						return 0;
					}
					
					public Message[] contents() {
						ArrayList<Message> messages = new ArrayList<Message>();
						for(String s : passNote.messages) {
							messages.add(createMessageFromText(s));
						}
						return messages.toArray(new Message[1]);
					}
				};
			}
			
			public Message createMessageFromText(String text) {
				return new ListItem(text, node);
			}

			public void requestDeletion(Message m) {
				ArrayList<String> toRemove = new ArrayList<String>();
				for(String msg : passNote.messages) {
					if(m.asText().equals(msg)) {
						toRemove.add(msg);
					}
				}
				for(String msg : toRemove) {
					passNote.messages.remove(msg);
				}
			}

			public String channelIdentifier() {
				return passNote.title;
			}

			public Account owningAccount() {
				// TODO Auto-generated method stub
				return owningAccount;
			}
		};
	}

	public Channel bySearchString(String nick, LayoutNode node) {
		for(ListpadNote n_iter : notes) {
			if(n_iter.title.equals(nick)) {
				return forNote(n_iter, node);
			}
		}
		ListpadNote newNote = new ListpadNote();
		newNote.title = nick;
		newNote.topic = "owo test time";
		newNote.messages.add("hello this is");
		newNote.messages.add("the default data");
		newNote.messages.add("for a notepad instance");
		notes.add(newNote);
		return forNote(newNote, node);
	}

	public Protocol owningProtocol() {
		return parentProtocol;
	}

}
