package io.gitlab.dahlterm.implementation.protocols.listpad;

import java.awt.Component;
import java.util.UUID;

import io.gitlab.dahlterm.abstractions.Action;
import io.gitlab.dahlterm.abstractions.LayoutNode;
import io.gitlab.dahlterm.abstractions.ObjectCapabilities;
import io.gitlab.dahlterm.abstractions.events.EventReciever;
import io.gitlab.dahlterm.abstractions.protocol.Message;

public class ListItem implements Message {
	
	String messageText = "error: no message here";
	LayoutNode containerNode;

	public ListItem(String text, LayoutNode node) {
		messageText = text;
		containerNode = node;
	}
	
	public Action[] properties() {
		Action delete = new Action() {

			public String actionTitle() {
				return "delete message";
			}

			public void performAction(ObjectCapabilities target) {
				Message msg = (Message) target;
				
				LayoutNode container = msg.container();
				container.endpoint().requestDeletion(msg);
				updater.receiveEvent(null);
			}

			public String helpInformation() {
				return "deletes the message from the notepad";
			}

			public UUID identifier() {
				return null;
			}
			
		};
		
		return new Action[] { delete };
	}

	public Component customComponent() {
		return null;
	}

	public String asText() {
		return messageText;
	}

	public LayoutNode container() {
		return containerNode;
	}
	EventReciever updater;
	public void setUpdateReciever(EventReciever er) {
		updater = er;
	}

}
