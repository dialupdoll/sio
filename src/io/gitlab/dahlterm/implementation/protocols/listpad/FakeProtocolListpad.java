package io.gitlab.dahlterm.implementation.protocols.listpad;

import java.awt.Component;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.ListIterator;
import java.util.UUID;
import java.util.concurrent.ArrayBlockingQueue;

import javax.swing.JLabel;
import javax.swing.JOptionPane;

import com.grack.nanojson.JsonAppendableWriter;
import com.grack.nanojson.JsonArray;
import com.grack.nanojson.JsonObject;
import com.grack.nanojson.JsonParser;
import com.grack.nanojson.JsonParserException;
import com.grack.nanojson.JsonWriter;

import io.gitlab.dahlterm.abstractions.LayoutNode;
import io.gitlab.dahlterm.abstractions.events.AccountRegisteredEventReciever;
import io.gitlab.dahlterm.abstractions.protocol.Account;
import io.gitlab.dahlterm.abstractions.protocol.Protocol;
import io.gitlab.dahlterm.implementation.PersistentData;
import io.gitlab.dahlterm.implementation.protocols.listpad.FakeAccountListpad.ListpadNote;

public class FakeProtocolListpad implements Protocol {
	
	ArrayList<Account> accounts = new ArrayList<Account>();

	public String nickname() {
		return "notepad";
	}

	public void authorizeNewAccount(AccountRegisteredEventReciever arer) {
		Account result = (Account)new FakeAccountListpad(this);
		if(arer != null)
			arer.accountRegistered(result);
		accounts.add(result);
	}

	public String protocolShortDescription() {
		return "a simple note or list based pad of sorts. used for testing the user interface.";
	}

	public final String keyNoteSection = "saved_notes", 
			keyNoteTitle = "note_title",
			keyNoteTopic = "note_topic",
			keyNoteMessages = "note_messages";
	
	public Account loadRegisteredAccountFromDisk(String folder) {
		FakeAccountListpad account = new FakeAccountListpad(this);
		try {
			FileInputStream fis = new FileInputStream(folder + File.separator + "account config" + ".sio.json");
			JsonObject obj =  JsonParser.object().from(fis);
			if(obj==null) {
				System.out.println("huh?");
			}
			account.accountName = obj.getString(PersistentData.keyAccountNickname);
			account.ident = UUID.fromString(obj.getString(PersistentData.keyAccountUUID));
			JsonArray notes = obj.getArray(keyNoteSection);

			ListIterator<Object> iterator = notes.listIterator();

			while(iterator.hasNext()) {

				JsonObject note = (JsonObject) iterator.next();
				
				ListpadNote ln_note = account.create();
				ln_note.title = note.getString(keyNoteTitle);
				ln_note.topic = note.getString(keyNoteTopic);
				JsonArray messages = note.getArray(keyNoteMessages);

				ListIterator<Object> m_iterator = messages.listIterator();

				while(m_iterator.hasNext()) {
					String s = (String)m_iterator.next();
					ln_note.messages.add(s);
				}
				account.notes.add(ln_note);
			}
			
			fis.close();
			
		} catch (JsonParserException e) {
			e.printStackTrace();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		accounts.add(account);
		return account;
	}

	public void saveToFile(Account acc, String folder) {
		FileOutputStream fileWriter;
		try {
			fileWriter = new FileOutputStream(folder + File.separator + "account config" + ".sio.json", false);
			FakeAccountListpad account = (FakeAccountListpad)acc;

			JsonAppendableWriter jsonWriter = JsonWriter.indent("\t").on(fileWriter);
			jsonWriter.object();
				jsonWriter.value(PersistentData.keyAccountNickname, account.nickname());
				jsonWriter.value(PersistentData.keyAccountUUID, account.identifier().toString());
				jsonWriter.value(PersistentData.keyAccountProtocol, account.owningProtocol().nickname());
				jsonWriter.value(PersistentData.keyAccountProtocolUUID, account.owningProtocol().identifier().toString());
				
				jsonWriter.array(keyNoteSection);
					for(FakeAccountListpad.ListpadNote note : account.notes) {
						jsonWriter.object();
							jsonWriter.value(keyNoteTitle, note.title);
							jsonWriter.value(keyNoteTopic, note.topic);
							jsonWriter.array(keyNoteMessages);
								for(String s : note.messages) {
									jsonWriter.value(s);
								}
							jsonWriter.end();
						jsonWriter.end();
					}
				jsonWriter.end();
			jsonWriter.end();
			jsonWriter.done();
			
			fileWriter.flush();
			fileWriter.close();
			
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
	}

	public Component protocolSettingsConfigurationMenu() {
		return new JLabel("left unimplemented! there isn't really anything to configure in the example protocol.");
	}
	
	UUID ident = UUID.fromString("ddfb1220-4d66-4906-8866-63afd28a6e25");

	public UUID identifier() {
		return ident;
	}

	public Account bySearchString(String nick, LayoutNode node) {
		Account result = null;
		System.out.println(accounts.size());
		for(Account acc_iter : accounts) {
			System.out.println(acc_iter.toString());
			System.out.println(acc_iter.identifier().toString());
			if(acc_iter.nickname().equals(nick)) {
				if(result != null) {
					if(result != acc_iter) {
						JOptionPane.showMessageDialog(null, "Multiple accounts exist with that nickname! Use identifiers instead, to avoid this error.", "Duplicate Account Nicknames!", JOptionPane.ERROR_MESSAGE);
						return null;
					}
				} else {
					result = acc_iter;
				}
			} else {
				try {
					if(acc_iter.identifier().equals(UUID.fromString(nick))) {
						return acc_iter;
						/// the chances of a collision are RIDONKULOUSLY low.
					}
				} catch (IllegalArgumentException ile) {
					/// this is expected! do nothing. this just means we didn't pass in a UUID format, because it wasn't one.
				}
			}
		}
		
		return result;
	}
}
