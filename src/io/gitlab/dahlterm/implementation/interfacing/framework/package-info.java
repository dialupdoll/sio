package io.gitlab.dahlterm.implementation.interfacing.framework;

/*
 * the framework package contains non-protocol interface code, specifically static things like the dashboard's
 * wrapping code that contains overridable configuration data.
 */