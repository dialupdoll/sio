package io.gitlab.dahlterm.abstractions.protocol;

import java.awt.Component;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.UUID;

import io.gitlab.dahlterm.abstractions.LayoutNode;
import io.gitlab.dahlterm.abstractions.ObjectCapabilities;
import io.gitlab.dahlterm.abstractions.events.EventReciever;

//Account extends ObjectCapabilities to allow protocol-specific, per-account preferences, and settings. this can be client-specific, such as disabling the favorite button, or something relevant to the protocol or account in question, like changing your password or changing your profile picture
public interface Account extends ObjectCapabilities {
	public Channel[] availableChannels();
	public String nickname();
	public UUID identifier();
	// below: preconfigured slot-in layoutnodes (with children, sometimes) that act as common functions in an account, like a list of all accounts you have friended, or all the people you follow
	public ArrayList<LayoutNode> defaultHierarchys();
	//TODO: allow for accounts to have images / profile pictures
	
	public Component accountSettingsConfigurationMenu(EventReciever uer);
	public Protocol owningProtocol();
	
	public Channel bySearchString(String nick, LayoutNode node);
}
