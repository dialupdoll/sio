/**
 *	the "generic" package title that describes where you will find this package hosted
 *	which is just on gitlab under my username. really nothing special about this package,
 *	it's just the starting point for the project.
 */
package io.gitlab.dahlterm;